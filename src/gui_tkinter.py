import tkinter as tk
from tkinter import messagebox as mb
from threading import Thread
import solver_api

class SudokuGui():
    backend_url = "http://127.0.0.1:5000/postjson"

    def __init__(self):

        def initialize_window(self):
            self.root = tk.Tk()
            label_text = tk.Label(self.root, text="Sudoku solver application",
                                  fg="black", bg="red", anchor="w")
            label_text.grid(column=0, row=0, columnspan=9)

        def initialize_input_fields(self):
            self.input_fields = []
            for i in range(0, 9):
                for j in range(0, 9):
                    new_cell = GuiCell(self.root, j, i)
                    self.input_fields.append(new_cell)

        def initialize_buttons(self):
            button_solve = tk.Button(self.root, text="Solve sudoku", bg="blue",
                                     fg="white", command=self.solve_the_game)
            button_solve.grid(column=0, row=10, columnspan=9)
            button_clean = tk.Button(self.root, text="Clean board",
                                     bg="blue", fg="white", command=self.clean_the_board)
            button_clean.grid(column=0, row=11, columnspan=9)

        def launch_the_gui_and_backend(self):
            self.launch_backend_application()
            self.root.mainloop()

        initialize_window(self)
        initialize_input_fields(self)
        initialize_buttons(self)
        launch_the_gui_and_backend(self)

    def solve_the_game(self):

        def get_the_users_inputs(self):
            result = ""
            for i in self.input_fields:
                if i.row == 8 and i.column == 8:
                    result += i.get()
                elif i.row == 8:
                    result += i.get()+";"
                else:
                    result += i.get()+","
            return result

        def send_users_inputs_to_the_api(self, user_inputs):
            import requests
            response = requests.post(self.backend_url, json={"key1": user_inputs})
            return response

        def update_the_gui_with_the_result_of_the_api(self, response):

            def exit_status_of_the_request_is_ok(response):
                return response.status_code == 200

            def show_error(self):
                mb.showerror("Answer", "Sorry, no solution available")

            def clean_the_response(response):
                response_text = response.text
                text_without_whitespace = response_text.strip().replace(" ", "")
                text_without_whitespace_nor_new_lines = text_without_whitespace.replace("\n", "")
                return text_without_whitespace_nor_new_lines

            def update_the_gui(self, response_text):
                for number, cell in zip(response_text, self.input_fields):
                    print(number)
                    print(cell.row, cell.column)
                    cell.delete(0)
                    cell.insert(0, number)

            if exit_status_of_the_request_is_ok(response):
                cleaned_response = clean_the_response(response)
                update_the_gui(self, cleaned_response)
            else:
                show_error(self)
                self.clean_the_board()

        users_inputs = get_the_users_inputs(self)
        result = send_users_inputs_to_the_api(self, users_inputs)
        update_the_gui_with_the_result_of_the_api(self, result)

    def clean_the_board(self):

        for i in self.input_fields:
            i.delete(0)
            i.insert(0, "0")

    def launch_backend_application(self):

        thread = Thread(target=solver_api.run_app_for_graphical_interface)
        thread.start()
        print("thread finished...exiting")

class GuiCell(tk.Entry):
    def __init__(self, root, row, column, default=0, width=3):
        super().__init__(root, width=width)
        self.grid(row=row+1, column=column)
        self.insert(0, default)
        self.row = row
        self.column = column

if __name__ == "__main__":
    gui = SudokuGui()
