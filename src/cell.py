import sudoku

class Cell:

    POSSIBLE_VALUES = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    UNDEFINED_VALUE = 0
    MAX_I = 8
    MAX_J = 8
    MIN_I = 0
    MIN_J = 0

    def __init__(self, i, j, table="optional"):
        if i > Cell.MAX_I or j > Cell.MAX_J or not isinstance(i, int) or not isinstance(j, int):
            raise Exception()

        if not table == "optional":
            self.table = table

        self.i = i
        self.j = j

    def set_initial_conditions(self, value):
        if value == Cell.UNDEFINED_VALUE:
            self.value = Cell.UNDEFINED_VALUE
            self.is_fixed = False
        else:
            self.value = value
            self.is_fixed = True

    def __is_this_my_previous_cell(self, previous_cell):
        if self.i == Cell.MIN_I and self.j == Cell.MIN_J:
            if previous_cell == None:
                return True
            else:
                return False
        if self.j == previous_cell.j:
            if (self.i - previous_cell.i) == 1:
                return True
            else:
                return False
        if (self.j - previous_cell.j) == 1 and previous_cell.i == Cell.MAX_I:
            return True

        return false

    def set_previous_cell(self, previous_cell):
        if self.__is_this_my_previous_cell(previous_cell):
            self.previous_cell = previous_cell
        else:
            raise Exception()

    def __is_this_my_next_cell(self, next_cell):
        if self.j == Cell.MAX_J and self.i == Cell.MAX_I and next_cell == None:
            return True
        if self.j == next_cell.j:
            if (next_cell.i - self.i) == 1:
                return True
            else:
                return False
        if self.i == Cell.MAX_I and next_cell.i == Cell.MIN_I:
            if (next_cell.j - self.j) == 1:
                return True
            else:
                return False

    def set_next_cell(self, next_cell):
        if self.__is_this_my_next_cell(next_cell):
            self.next_cell = next_cell
        else:
            raise Exception()

    def is_this_value_feasible_for_row_column_and_square(self,value):
        return self.table.is_this_value_feasible_for_column_i(value, self.i) and self.table.is_this_value_feasible_for_row_j(value, self.j) and self.table.is_this_value_feasible_for_square_ij(value, self.i, self.j)

    def fixed_cell_is_this_value_feasible_for_row_column_and_square(self):
        value = self.value
        self.value = Cell.UNDEFINED_VALUE
        value_is_feasible = self.table.is_this_value_feasible_for_column_i(value, self.i) and self.table.is_this_value_feasible_for_row_j(value, self.j) and self.table.is_this_value_feasible_for_square_ij(value, self.i, self.j)
        self.value = value
        return value_is_feasible
    def set_next_feasible_value(self):
        if self.is_fixed:
            return

        while len(self.possible_values)>0:
            value = self.possible_values.pop()
            if self.is_this_value_feasible_for_row_column_and_square(value):
                self.value = value
                return

        self.value = Cell.UNDEFINED_VALUE
        raise Exception()

    def __try_next_value(self):
        self.set_next_feasible_value()
        if self.i == Cell.MAX_I and self.j == Cell.MAX_J:
            return
        try:
            self.next_cell.solve()
        except Exception:
            self.__try_next_value()

    def __solve_fixed_cell(self):
        if self.i == Cell.MAX_I and self.j == Cell.MAX_J:
            return
        if self.fixed_cell_is_this_value_feasible_for_row_column_and_square():
            self.next_cell.solve()
        else:
            text = "{0} ({1} {2})".format(self.value,self.i,self.j)
            raise RuntimeError(text)

    def initilize_possible_values(self):
        self.value = Cell.UNDEFINED_VALUE
        self.possible_values = Cell.POSSIBLE_VALUES.copy()

    def __solve_variable_cell(self):
        self.initilize_possible_values()
        self.__try_next_value()

    def solve(self):
        if self.is_fixed:
            self.__solve_fixed_cell()
        else:
            self.__solve_variable_cell()
