
import unittest
from sudoku import *
from cell import *

class TestCell(unittest.TestCase):
 
    def test_init(self):
        test_cell = Cell(1,2)
        self.assertEqual(test_cell.i, 1)
        self.assertEqual(test_cell.j, 2)
        with self.assertRaises(Exception):
           Cell(10,1)
        with self.assertRaises(Exception):
           Cell(1.5,1)
        with self.assertRaises(Exception):
           Cell(1,1.5)

    def test_cell_set_initial_conditions1(self):
        test_cell = Cell(1,2)
        test_cell.set_initial_conditions(0)
        self.assertEqual(test_cell.is_fixed, False)
    def test_cell_set_initial_conditions2(self):
        test_cell = Cell(1,2)
        test_cell.set_initial_conditions(1)
        self.assertEqual(test_cell.is_fixed, True)
    def test_cell_set_initial_conditions3(self):
        test_cell = Cell(1,2)
        test_cell.set_initial_conditions(0)
        self.assertEqual(test_cell.value, 0)

    def test_cell_set_previous_cell(self):
        test_cell = Cell(1,0)
        test_previous_cell = Cell(0,0)
        test_cell.set_previous_cell(test_previous_cell)
        self.assertEqual(test_cell.previous_cell, test_previous_cell)

    def test_cell_set_previous_cell2(self):
        test_cell = Cell(1,0)
        test_not_a_previous_cell = Cell(2,0)
        with self.assertRaises(Exception):
            test_cell.set_previous_cell(test_not_a_previous_cell)

    def test_cell_set_previous_cell4(self):
        test_cell = Cell(0,0)
        test_cell.set_previous_cell(None)
        self.assertEqual(test_cell.previous_cell, None)

    def test_cell_set_previous_cell5(self):
        test_cell = Cell(0,0)
        with self.assertRaises(Exception):
            test_cell.set_previous_cell(Cell(1,0))

    def test_cell_set_previous_cell6(self):
        test_cell = Cell(1,1)
        test_not_a_previous_cell = Cell(2,1)
        with self.assertRaises(Exception):
            test_cell.set_previous_cell(test_not_a_previous_cell)

    def test_cell_set_previous_cell7(self):
        test_cell = Cell(1,1)
        test_previous_cell = Cell(8,0)
        test_cell.set_previous_cell(test_previous_cell)
        self.assertEqual(test_cell.previous_cell, test_previous_cell)

    def test_cell_set_previous_cell8(self):
        test_cell = Cell(8,0)
        test_not_a_previous_cell = Cell(8,1)
        with self.assertRaises(Exception):
            test_cell.set_previous_cell(test_not_a_previous_cell)

    def test_cell_set_next_cell(self):
        test_cell = Cell(0,0)
        test_next_cell = Cell(1,0)
        test_cell.set_next_cell(test_next_cell)
        self.assertEqual(test_cell.next_cell, test_next_cell)

    def test_cell_set_next_cell2(self):
        cell = Cell(1,0)
        not_next_cell = Cell(0,0)
        with self.assertRaises(Exception):
            cell.set_next_cell(not_next_cell)

    def test_cell_set_next_cell3(self):
        cell = Cell(8,8)
        cell.set_next_cell(None)
        self.assertEqual(cell.next_cell, None)

    def test_cell_set_next_cell4(self):
        cell = Cell(8,0)
        next_cell = Cell(0,1)
        cell.set_next_cell(next_cell)
        self.assertEqual(cell.next_cell, next_cell)

    def test_cell_set_next_cell5(self):
        cell = Cell(0,1)
        not_next_cell = Cell(0,1)
        with self.assertRaises(Exception):
            cell.set_next_cell(not_next_cell)

    def test_cell_set_next_cell5(self):
        cell = Cell(0,1)
        not_next_cell = Cell(2,2)
        with self.assertRaises(Exception):
            cell.set_next_cell(not_next_cell)

    def test_create_the_sudoku_table(self):
        initial_conditions = [[8,0,2,0,5,0,7,0,1],
                              [0,0,7,0,8,2,4,6,0],
                              [0,1,0,9,0,0,0,0,0],
                              [6,0,0,0,0,1,8,3,2],
                              [5,0,0,0,0,0,0,0,9],
                              [1,8,4,3,0,0,0,0,6],
                              [0,0,0,0,0,4,0,2,0],
                              [0,9,5,6,1,0,3,0,0],
                              [3,0,8,0,9,0,6,0,7]]
        sudoku_table = SudokuTable(initial_conditions)
        self.assertEqual(len(sudoku_table.game_table), 9)
        for row in sudoku_table.game_table:
            self.assertEqual(len(row), 9)

    @unittest.skip("Skipping because I was not running this one before.")
    def test_get_game_table_row_j(self):
        initial_conditions = [[8,0,2,0,5,0,7,0,1],
                              [0,0,7,0,8,2,4,6,0],
                              [0,1,0,9,0,0,0,0,0],
                              [6,0,0,0,0,1,8,3,2],
                              [5,0,0,0,0,0,0,0,9],
                              [1,8,4,3,0,0,0,0,6],
                              [0,0,0,0,0,4,0,2,0],
                              [0,9,5,6,1,0,3,0,0],
                              [3,0,8,0,9,0,6,0,7]]
        sudoku_table = SudokuTable(initial_conditions)
        game_table_0 = sudoku_table.game_table[0]
        self.assertEqual(sudoku_table.get_game_table_row_j(0), game_table_0)

    def test_get_game_table_row_j_2(self):
        initial_conditions = [[8,0,2,0,5,0,7,0,1],
                              [0,0,7,0,8,2,4,6,0],
                              [0,1,0,9,0,0,0,0,0],
                              [6,0,0,0,0,1,8,3,2],
                              [5,0,0,0,0,0,0,0,9],
                              [1,8,4,3,0,0,0,0,6],
                              [0,0,0,0,0,4,0,2,0],
                              [0,9,5,6,1,0,3,0,0],
                              [3,0,8,0,9,0,6,0,7]]
        sudoku_table = SudokuTable(initial_conditions)
        row_0 = sudoku_table.get_game_table_row_j(0)
        for cell in row_0:
            self.assertEqual(cell.j, 0)

    def test_get_game_table_column_i(self):
        initial_conditions = [[8,0,2,0,5,0,7,0,1],
                              [0,0,7,0,8,2,4,6,0],
                              [0,1,0,9,0,0,0,0,0],
                              [6,0,0,0,0,1,8,3,2],
                              [5,0,0,0,0,0,0,0,9],
                              [1,8,4,3,0,0,0,0,6],
                              [0,0,0,0,0,4,0,2,0],
                              [0,9,5,6,1,0,3,0,0],
                              [3,0,8,0,9,0,6,0,7]]
        sudoku_table = SudokuTable(initial_conditions)
        column_0 = sudoku_table.get_game_table_column_i(0)
        for cell in column_0:
            self.assertEqual(cell.i, 0)

    def test_set_the_initial_conditions(self):
        initial_conditions = [[0,4,0,0,0,0,1,7,9],
                              [0,0,2,0,0,8,0,5,4],
                              [0,0,6,0,0,5,0,0,8],
                              [0,8,0,0,7,0,9,1,0],
                              [0,5,0,0,9,0,0,3,0],
                              [0,1,9,0,6,0,0,4,0],
                              [3,0,0,4,0,0,7,0,0],
                              [5,7,0,1,0,0,2,0,0],
                              [9,2,8,0,0,0,0,6,0]]
        sudoku_table = SudokuTable(initial_conditions)
        for row in sudoku_table.game_table:
            for cell in row:
                self.assertTrue(cell.is_fixed or True)
                self.assertTrue(cell.is_fixed or True)

    def test_get_previous_cell_by_ij(self):
        initial_conditions = [[8,0,2,0,5,0,7,0,1],
                              [0,0,7,0,8,2,4,6,0],
                              [0,1,0,9,0,0,0,0,0],
                              [6,0,0,0,0,1,8,3,2],
                              [5,0,0,0,0,0,0,0,9],
                              [1,8,4,3,0,0,0,0,6],
                              [0,0,0,0,0,4,0,2,0],
                              [0,9,5,6,1,0,3,0,0],
                              [3,0,8,0,9,0,6,0,7]]
        sudoku_table = SudokuTable(initial_conditions)
        self.assertEqual(sudoku_table.get_previous_cell_by_ij(0,0), None)
        self.assertEqual(sudoku_table.get_previous_cell_by_ij(0,1).i, 8)
        self.assertEqual(sudoku_table.get_previous_cell_by_ij(0,1).j, 0)
        self.assertEqual(sudoku_table.get_previous_cell_by_ij(1,0).i, 0)
        self.assertEqual(sudoku_table.get_previous_cell_by_ij(1,0).j, 0)

    def test_get_next_cell_by_ij(self):
        initial_conditions = [[0,4,0,0,0,0,1,7,9],
                              [0,0,2,0,0,8,0,5,4],
                              [0,0,6,0,0,5,0,0,8],
                              [0,8,0,0,7,0,9,1,0],
                              [0,5,0,0,9,0,0,3,0],
                              [0,1,9,0,6,0,0,4,0],
                              [3,0,0,4,0,0,7,0,0],
                              [5,7,0,1,0,0,2,0,0],
                              [9,2,8,0,0,0,0,6,0]]
        sudoku_table = SudokuTable(initial_conditions)

        self.assertEqual(sudoku_table.get_next_cell_by_ij(8,8), None)
        self.assertEqual(sudoku_table.get_next_cell_by_ij(8,0).i, 0)
        self.assertEqual(sudoku_table.get_next_cell_by_ij(8,0).j, 1)
        self.assertEqual(sudoku_table.get_next_cell_by_ij(1,0).i, 2)
        self.assertEqual(sudoku_table.get_next_cell_by_ij(1,0).j, 0)

    def test_connect_the_cells(self):
        initial_conditions = [[0,4,0,0,0,0,1,7,9],
                              [0,0,2,0,0,8,0,5,4],
                              [0,0,6,0,0,5,0,0,8],
                              [0,8,0,0,7,0,9,1,0],
                              [0,5,0,0,9,0,0,3,0],
                              [0,1,9,0,6,0,0,4,0],
                              [3,0,0,4,0,0,7,0,0],
                              [5,7,0,1,0,0,2,0,0],
                              [9,2,8,0,0,0,0,6,0]]
        sudoku_table = SudokuTable(initial_conditions)
        for row in sudoku_table.game_table:
            for cell in row:
                if (cell.i == 8 and cell.j == 8) or (cell.i == 0 and cell.j == 0):
                    continue            
                self.assertIsInstance(cell.previous_cell, Cell)
                self.assertIsInstance(cell.next_cell, Cell)

    def test_connect_the_cells_2(self):
        initial_conditions = [[0,4,0,0,0,0,1,7,9],
                              [0,0,2,0,0,8,0,5,4],
                              [0,0,6,0,0,5,0,0,8],
                              [0,8,0,0,7,0,9,1,0],
                              [0,5,0,0,9,0,0,3,0],
                              [0,1,9,0,6,0,0,4,0],
                              [3,0,0,4,0,0,7,0,0],
                              [5,7,0,1,0,0,2,0,0],
                              [9,2,8,0,0,0,0,6,0]]
        sudoku_table = SudokuTable(initial_conditions)
        current_cell = sudoku_table.get_cell_ij(0,0) 
        counter = 0
        while counter < 81:
            if current_cell.i == 8 and current_cell.j == 8:
                self.assertTrue(True)
                break
            current_cell = current_cell.next_cell
            counter = counter + 1
        else:
            self.assertTrue(False)

    def test_convert_cell_list_to_value_list(self):

        cell1 = Cell(0,0)
        cell1.value=1
        cell2 = Cell(1,0)
        cell2.value=5
        cell3 = Cell(2,0)
        cell3.value=9
        game_table_list = [cell1, cell2, cell3]
        result = sudoku.SudokuTable.convert_cell_list_to_value_list(game_table_list)
        self.assertEqual(result, [1,5,9])

    def test_is_this_value_feasible_for_row_j(self):
        test_escenario = [[0,4,0,0,0,0,1,7,9],
            [0,0,2,0,0,8,0,5,4],
            [0,0,6,0,0,5,0,0,8],
            [0,8,0,0,7,0,9,1,0],
            [0,5,0,0,9,0,0,3,0],
            [0,1,9,0,6,0,0,4,0],
            [3,0,0,4,0,0,7,0,0],
            [5,7,0,1,0,0,2,0,0],
            [9,2,8,0,0,0,0,6,0]]
        sudoku_table = SudokuTable(test_escenario)

        self.assertTrue( sudoku_table.is_this_value_feasible_for_row_j(value=5, j=0))
        self.assertTrue( sudoku_table.is_this_value_feasible_for_row_j(value=3, j=1))
        self.assertTrue( sudoku_table.is_this_value_feasible_for_row_j(value=2, j=2))
        self.assertTrue( sudoku_table.is_this_value_feasible_for_row_j(value=2, j=0))
        self.assertFalse(sudoku_table.is_this_value_feasible_for_row_j(value=9, j=0))
        self.assertFalse(sudoku_table.is_this_value_feasible_for_row_j(value=2, j=1))
        self.assertFalse(sudoku_table.is_this_value_feasible_for_row_j(value=6, j=2))

    def test_is_this_value_feasible_for_column_i(self):
        test_escenario = [[0,4,0,0,0,0,1,7,9],
            [0,0,2,0,0,8,0,5,4],
            [0,0,6,0,0,5,0,0,8],
            [0,8,0,0,7,0,9,1,0],
            [0,5,0,0,9,0,0,3,0],
            [0,1,9,0,6,0,0,4,0],
            [3,0,0,4,0,0,7,0,0],
            [5,7,0,1,0,0,2,0,0],
            [9,2,8,0,0,0,0,6,0]]
        sudoku_table = SudokuTable(test_escenario)

        self.assertTrue( sudoku_table.is_this_value_feasible_for_column_i(value=2, i=0))
        self.assertTrue( sudoku_table.is_this_value_feasible_for_column_i(value=3, i=1))
        self.assertTrue( sudoku_table.is_this_value_feasible_for_column_i(value=5, i=2))
        self.assertTrue( sudoku_table.is_this_value_feasible_for_column_i(value=2, i=3))
        self.assertFalse(sudoku_table.is_this_value_feasible_for_column_i(value=3, i=0))
        self.assertFalse(sudoku_table.is_this_value_feasible_for_column_i(value=8, i=1))
        self.assertFalse(sudoku_table.is_this_value_feasible_for_column_i(value=2, i=2))

    def test_get_values_of_square_where_ij_belong(self):
        test_escenario = [[0,4,0,0,0,0,1,7,9],
                          [0,0,2,0,0,8,0,5,4],
                          [0,0,6,0,0,5,0,0,8],
                          [0,8,0,0,7,0,9,1,0],
                          [0,5,0,0,9,0,0,3,0],
                          [0,1,9,0,6,0,0,4,0],
                          [3,0,0,4,0,0,7,0,0],
                          [5,7,0,1,0,0,2,0,0],
                          [9,2,8,0,0,0,0,6,0]]
        sudoku_table = SudokuTable(test_escenario)

        self.assertEqual(set(sudoku_table.get_values_of_square_where_ij_belong(i=0, j=0)), set([0,4,2,6]))
        self.assertEqual(set(sudoku_table.get_values_of_square_where_ij_belong(i=3, j=3)), set([0,6,7,9]))
        self.assertEqual(set(sudoku_table.get_values_of_square_where_ij_belong(i=6, j=6)), set([0,2,6,7]))
        self.assertEqual(set(sudoku_table.get_values_of_square_where_ij_belong(i=0, j=8)), set([0,2,3,5,7,8,9]))
        self.assertEqual(set(sudoku_table.get_values_of_square_where_ij_belong(i=8, j=0)), set([0,1,7,9,5,4,8]))
        self.assertEqual(set(sudoku_table.get_values_of_square_where_ij_belong(i=3, j=0)), set([0,8,5]))
        self.assertEqual(set(sudoku_table.get_values_of_square_where_ij_belong(i=6, j=4)), set([0,1,3,4,9]))
        self.assertEqual(set(sudoku_table.get_values_of_square_where_ij_belong(i=5, j=7)), set([0,1,4]))
        self.assertEqual(set(sudoku_table.get_values_of_square_where_ij_belong(i=2, j=2)), set([0,2,4,6]))
        self.assertEqual(set(sudoku_table.get_values_of_square_where_ij_belong(i=2, j=7)), set([0,2,3,5,7,8,9]))

    def test_is_this_value_feasible_for_square_ij(self):
        test_escenario = [[0,4,0,0,0,0,1,7,9],
            [0,0,2,0,0,8,0,5,4],
            [0,0,6,0,0,5,0,0,8],
            [0,8,0,0,7,0,9,1,0],
            [0,5,0,0,9,0,0,3,0],
            [0,1,9,0,6,0,0,4,0],
            [3,0,0,4,0,0,7,0,0],
            [5,7,0,1,0,0,2,0,0],
            [9,2,8,0,0,0,0,6,0]]
        sudoku_table = SudokuTable(test_escenario)
        self.assertTrue( sudoku_table.is_this_value_feasible_for_square_ij(value=5,  i=0, j=0))
        self.assertTrue( sudoku_table.is_this_value_feasible_for_square_ij(value=3,  i=1, j=4))
        self.assertTrue( sudoku_table.is_this_value_feasible_for_square_ij(value=4,  i=2, j=8))
        self.assertTrue( sudoku_table.is_this_value_feasible_for_square_ij(value=2,  i=3, j=0))
        self.assertFalse(sudoku_table.is_this_value_feasible_for_square_ij(value=4, i=0, j=0))
        self.assertFalse(sudoku_table.is_this_value_feasible_for_square_ij(value=5, i=1, j=4))
        self.assertFalse(sudoku_table.is_this_value_feasible_for_square_ij(value=2, i=2, j=8))

    def test_cell_get_next_feasible_value(self):
        test_escenario = [[0,4,0,0,0,0,1,7,9],
                          [0,0,2,0,0,8,0,5,4],
                          [0,0,6,0,0,5,0,0,8],
                          [0,8,0,0,7,0,9,1,0],
                          [0,5,0,0,9,0,0,3,0],
                          [0,1,9,0,6,0,0,4,0],
                          [3,0,0,4,0,0,7,0,0],
                          [5,7,0,1,0,0,2,0,0],
                          [9,2,8,0,0,0,0,6,0]]
        sudoku_table = SudokuTable(test_escenario)
        cell_00 = sudoku_table.get_cell_ij(0,0)
        cell_00.initilize_possible_values()
        cell_00.set_next_feasible_value()
        self.assertEqual(cell_00.value, 8)

    def test_cell_get_next_feasible_value_2(self):
        test_escenario = [[0,4,8,0,0,0,1,7,9],
                          [0,0,2,0,0,8,0,5,4],
                          [0,0,6,0,0,5,0,0,8],
                          [0,8,0,0,7,0,9,1,0],
                          [0,5,0,0,9,0,0,3,0],
                          [0,1,9,0,6,0,0,4,0],
                          [3,0,0,4,0,0,7,0,0],
                          [5,7,0,1,0,0,2,0,0],
                          [9,2,8,0,0,0,0,6,0]]
        sudoku_table = SudokuTable(test_escenario)
        cell_00 = sudoku_table.get_cell_ij(0,0)
        with self.assertRaises(Exception):
           cell_00.set_next_feasible_value()

    def test_cell_get_next_feasible_value_3(self):
        test_escenario = [[0,4,0,0,0,0,1,7,9],
                          [0,0,2,0,0,8,0,5,4],
                          [0,0,6,0,0,5,0,0,8],
                          [0,8,0,0,7,0,9,1,0],
                          [0,5,0,0,9,0,0,3,0],
                          [0,1,9,0,6,0,0,4,0],
                          [3,0,0,4,0,0,7,0,0],
                          [5,7,0,1,0,0,2,0,0],
                          [9,2,8,0,0,0,0,6,0]]
        sudoku_table = SudokuTable(test_escenario)
        cell_10 = sudoku_table.get_cell_ij(1,0)
        cell_10.set_next_feasible_value()
        self.assertEqual(cell_10.value, 4)

    def test_solve(self):
        test_escenario = [[0,4,0,0,0,0,1,7,9],
                          [0,0,2,0,0,8,0,5,4],
                          [0,0,6,0,0,5,0,0,8],
                          [0,8,0,0,7,0,9,1,0],
                          [0,5,0,0,9,0,0,3,0],
                          [0,1,9,0,6,0,0,4,0],
                          [3,0,0,4,0,0,7,0,0],
                          [5,7,0,1,0,0,2,0,0],
                          [9,2,8,0,0,0,0,6,0]]
        solution = [[8,4,5,6,3,2,1,7,9],
                    [7,3,2,9,1,8,6,5,4],
                    [1,9,6,7,4,5,3,2,8],
                    [6,8,3,5,7,4,9,1,2],
                    [4,5,7,2,9,1,8,3,6],
                    [2,1,9,8,6,3,5,4,7],
                    [3,6,1,4,2,9,7,8,5],
                    [5,7,4,1,8,6,2,9,3],
                    [9,2,8,3,5,7,4,6,1]]
        sudoku_table = SudokuTable(test_escenario)
        sudoku_table.solve_the_sudoku_game()
        cell_88 = sudoku_table.get_cell_ij(8, 8) 
        self.assertEqual(cell_88.value, 1)


    def test_solve_2(self):
        unsolvable_test_escenario = [[1,1,0,0,0,0,0,0,0],
                           [0,0,0,0,0,0,0,0,0],
                           [0,0,0,0,0,0,0,0,0],
                           [0,0,0,0,0,0,0,0,0],
                           [0,0,0,0,0,0,0,0,0],
                           [0,0,0,0,0,0,0,0,0],
                           [0,0,0,0,0,0,0,0,0],
                           [0,0,0,0,0,0,0,0,0],
                           [0,0,0,0,0,0,0,0,0]]
        sudoku_table = SudokuTable(unsolvable_test_escenario)
        with self.assertRaises(RuntimeError):
            sudoku_table.solve_the_sudoku_game()

    def test_solve_3(self):
        solvable_test_escenario = [[1,0,0,0,0,0,0,0,0],
                           [0,0,0,0,0,0,0,0,0],
                           [0,0,0,0,0,0,0,0,0],
                           [0,0,0,0,0,0,0,0,0],
                           [0,0,0,0,0,0,0,0,0],
                           [0,0,0,0,0,0,0,0,0],
                           [0,0,0,0,0,0,0,0,0],
                           [0,0,0,0,0,0,0,0,0],
                           [0,0,0,0,0,0,0,0,0]]
        sudoku_table = SudokuTable(solvable_test_escenario)
        sudoku_table.solve_the_sudoku_game()
if __name__ == '__main__':
    unittest.main()
